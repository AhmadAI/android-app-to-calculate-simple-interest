package com.example.q3;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q3.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText edittext1, edittext2, edittext3;
        TextView textview5;
        Button computebtn;

        edittext1= (EditText) findViewById(R.id.edittext1);
        edittext2= (EditText) findViewById(R.id.edittext2);
        edittext3= (EditText) findViewById(R.id.edittext3);
        computebtn = (Button) findViewById(R.id.computebtn);
        textview5 = (TextView) findViewById(R.id.textview5);


        computebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int principal = Integer.parseInt(edittext1.getText().toString());
                int rate = Integer.parseInt(edittext2.getText().toString());
                int time = Integer.parseInt(edittext3.getText().toString());
                int si = (principal*rate*time)/100;

                textview5.setText("The simple interest is: "+si);


            }
        });

    }
}